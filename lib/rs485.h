/*
 * rs485.h
 *
 *  Created on: May 8, 2023
 *      Author: camilodiaz
 */

#ifndef LIB_RS485_H_
#define LIB_RS485_H_

#include "lib/timer.h"
//----------------------------------------------------------------------------
//--------------------------------- Defines ----------------------------------
//----------------------------------------------------------------------------

#define RS485_USE_UCA0
//#define RS485_USE_UCA1

#define RS485_RX_BUFFER_SIZE    64
#define RS485_TX_BUFFER_SIZE    16

#define RS485_RXTX_PIN          BIT0
#define RS485_RXTX_DIR          P2DIR
#define RS485_RXTX_OUT          P2OUT

#define RS485_STATE_IDLE           0
#define RS485_STATE_RECEIVING      1
#define RS485_STATE_TRANSMITTING   2

//----------------------------------------------------------------------------
//--------------------------------- Structure --------------------------------
//----------------------------------------------------------------------------

typedef struct rs485_control_t rs485_control_t;
struct rs485_control_t
{
    unsigned char flag;
    unsigned char state;

    unsigned char tx_pop;
    unsigned char tx_use;
    char tx_buffer[RS485_TX_BUFFER_SIZE];

    unsigned char rx_use;
    unsigned char rx_push;
    char rx_buffer[RS485_RX_BUFFER_SIZE];

    timer_control_t *tcp;
};

//----------------------------------------------------------------------------
//---------------------------------- General ---------------------------------
//----------------------------------------------------------------------------
void RS485_Init (rs485_control_t *rcp, timer_control_t *tcp);
void RS485_Task (rs485_control_t *rcp);

//----------------------------------------------------------------------------
//-------------------------------- Interfaces --------------------------------
//----------------------------------------------------------------------------

void RS485_Print(rs485_control_t *rcp,
                   unsigned char *bufer,
                   unsigned char length);


unsigned char RS485_Available(rs485_control_t *rcp);
unsigned char RS485_Busy(rs485_control_t *rcp);

void RS485_Read(rs485_control_t *rcp,
               unsigned char *buffer);

#endif /* LIB_RS485_H_ */
