/*
 * main.h
 *
 *  Created on: May 4, 2023
 *      Author: camilodiaz
 */

#ifndef MAIN_H_
#define MAIN_H_

#define DEVICE_TYPE         'h'//Header type
#define DEVICE_MASTER       'm'//Master device type
#define DEVICE_RESPONSE     'r'//Response message

#define FIRMWARE_VERSION    "rf1.02\n"

#endif /* MAIN_H_ */
