/*
 * rs485.c
 *
 *  Created on: May 8, 2023
 *      Author: camilodiaz
 */

#include "lib/rs485.h"
#include "lib/timer.h"
#include <msp430.h>

void RS485_Init (rs485_control_t *rcp, timer_control_t *tcp)
{
    rcp->tcp = tcp;

    RS485_RXTX_DIR |= RS485_RXTX_PIN;
    RS485_RXTX_OUT &= ~RS485_RXTX_PIN;//Tx -> 1, Rx -> 0

#ifdef RS485_USE_UCA1
    P2SEL0 |= BIT5 | BIT6;
    UCA1CTLW0 |= UCSWRST;
    UCA1CTLW0 |= UCSSEL__SMCLK;
    UCA1BR0 = 0x45;
    UCA1BR1 = 0x00;
    UCA1MCTLW = 0xAA00;
    UCA1CTLW0 &= ~UCSWRST;
    UCA1IE |= UCRXIE;
    //115200 bauds
#endif

#ifdef RS485_USE_UCA0
    P1SEL0 |= BIT6 | BIT7;                    // set 2-UART pin as second function
    UCA0CTLW0 |= UCSWRST;
    UCA0CTLW0 |= UCSSEL__SMCLK;
    UCA0BR0 = 0x45;
    UCA0BR1 = 0x00;
    UCA0MCTLW = 0xAA00;
    UCA0CTLW0 &= ~UCSWRST;
    UCA0IE |= UCRXIE;
    UCA0IE |= UCTXCPTIFG;
    //115200 bauds
#endif


};

void RS485_Task (rs485_control_t *rcp)
{
    switch (rcp->state)
    {
    case RS485_STATE_IDLE:
        if(rcp->rx_push > 0)
        {
            rcp->state = RS485_STATE_RECEIVING;
            Timer_Start(rcp->tcp, TIMER_RS485, 10);
        }
        break;
    case RS485_STATE_RECEIVING:
        if(rcp->rx_buffer[rcp->rx_push] == '\n')
        {
            rcp->rx_buffer[rcp->rx_push] = 0;
            rcp->rx_use += rcp->rx_push;
            rcp->rx_push = 0;
            rcp->state = RS485_STATE_IDLE;
        }
        if(Timer_Available(rcp->tcp, TIMER_RS485) != 0)
        {
            rcp->rx_push = 0;
            rcp->state = RS485_STATE_IDLE;
        }
        break;
    default:
        break;
    }
};

void RS485_Print(rs485_control_t *rcp,
                   unsigned char *bufer,
                   unsigned char length)
{
    unsigned char i;
    char *p;

    i = length;
    p = &rcp->tx_buffer[0];
    while(i--)
        *p++ = *bufer++;
    rcp->tx_use = length;
    rcp->state = RS485_STATE_TRANSMITTING;
    RS485_RXTX_OUT |= RS485_RXTX_PIN;

    #ifdef RS485_USE_UCA1
    UCA1IE |= UCTXIE;
    UCA1IFG |= UCTXIE;
#endif
#ifdef RS485_USE_UCA0
    UCA0IE |= UCTXIE;
    UCA0IFG |= UCTXIE;
#endif
};


unsigned char RS485_Available(rs485_control_t *rcp)
{
    return rcp->rx_use;
};

void RS485_Read(rs485_control_t *rcp,
               unsigned char *buffer)
{
    unsigned char i;
    char *p;
    i = rcp->rx_use;
    p = &rcp->rx_buffer[0];
    while(i--)
        *buffer++ = *p++;
    rcp->rx_use = 0;
};

unsigned char RS485_Busy(rs485_control_t *rcp)
{
    if(rcp->state != RS485_STATE_IDLE)
        return 1;
    return 0;
}

//---------------------------------- UART --------------------------------------
#ifdef RS485_USE_UCA1
#pragma vector=USCI_A1_VECTOR
__interrupt void USCI_A1_ISR(void)
#endif
#ifdef RS485_USE_UCA0
#pragma vector=USCI_A0_VECTOR
__interrupt void USCI_A0_ISR(void)
#endif
{
    extern rs485_control_t rcp;
#ifdef RS485_USE_UCA1
    switch(__even_in_range(UCA1IV,USCI_UART_UCTXCPTIFG))
#endif
#ifdef RS485_USE_UCA0
    switch(__even_in_range(UCA0IV,USCI_UART_UCTXCPTIFG))
#endif
    {
    case USCI_NONE:
        break;
    case USCI_UART_UCRXIFG:
#ifdef RS485_USE_UCA1
        rcp.rx_buffer[rcp.rx_push] = UCA1RXBUF;
#endif
#ifdef RS485_USE_UCA0
        rcp.rx_buffer[rcp.rx_push] = UCA0RXBUF;
#endif
        rcp.rx_push++;
        Timer_Restart(rcp.tcp, TIMER_RS485);
        break;
    case USCI_UART_UCTXIFG:
        if(rcp.tx_use == 0)
        {
#ifdef RS485_USE_UCA1
            UCA1IFG &= ~UCTXCPTIFG;
            UCA1IE |= UCTXCPTIE;
#endif
#ifdef RS485_USE_UCA0
            UCA0IFG &= ~UCTXCPTIFG;
            UCA0IE |= UCTXCPTIE;
#endif
            break;
        }
#ifdef RS485_USE_UCA1
        UCA1TXBUF = rcp.tx_buffer[rcp.tx_pop];
#endif
#ifdef RS485_USE_UCA0
        UCA0TXBUF = rcp.tx_buffer[rcp.tx_pop];
#endif
        rcp.tx_pop++;
        if(rcp.tx_pop == RS485_TX_BUFFER_SIZE) rcp.tx_pop = 0;
        rcp.tx_use--;
        break;
    case USCI_UART_UCSTTIFG:
        break;
    case USCI_UART_UCTXCPTIFG:
        rcp.state = RS485_STATE_IDLE;
#ifdef RS485_USE_UCA1
        UCA1IE &= ~UCTXCPTIE;
        UCA1IE &= ~UCTXIE;
#endif
#ifdef RS485_USE_UCA0
        UCA0IE &= ~UCTXCPTIE;
        UCA0IE &= ~UCTXIE;
#endif
        RS485_RXTX_OUT &= ~RS485_RXTX_PIN;
        break;
    default:
        break;
  }
}
