/*
 * timer.c
 *
 *  Created on: 25 ago. 2022
 *      Author: kamilo
 */

#include <msp430.h>
#include "lib/timer.h"

void Timer_Init (timer_control_t *tcp)
{
    TB0CCTL0 |= CCIE;                             // TBCCR0 interrupt enabled
    TB0CCR0 = 8000;
    TB0CTL = TBSSEL__SMCLK | MC__UP;             // SMCLK, UP mode
}


void Timer_Task (timer_control_t *tcp)
{
    char i;
    if(tcp->tick>0)
    {
        tcp->tick--;
        i = TIMER_SIZE;
        while(i--)
        {
            if((tcp->timer[i].flags&TIMER_B_ENABLE) != 0)
            {
                tcp->timer[i].count++;
                if(tcp->timer[i].count == tcp->timer[i].limit)
                {
                    tcp->timer[i].flags |= TIMER_B_FINISH;
                    tcp->timer[i].count = 0;
                    tcp->timer[i].flags &= ~TIMER_B_ENABLE;
                }
            }
        }
    }
}

#pragma vector = TIMER0_B0_VECTOR
__interrupt void Timer0_B0_ISR (void)
{
    extern timer_control_t tcp;
    tcp.tick++;
}


void Timer_Start(timer_control_t *tcp, unsigned char id, unsigned int time)
{
    tcp->timer[id].limit = time;
    tcp->timer[id].flags |= TIMER_B_ENABLE;
}

unsigned char Timer_Available(timer_control_t *tcp, unsigned char id)
{
    if((tcp->timer[id].flags&TIMER_B_FINISH) == 0)
        return 0;
    tcp->timer[id].flags &= ~TIMER_B_FINISH;
    return 1;
}

